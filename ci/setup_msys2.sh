#!/bin/bash

set -e

# Ensure packages and clean all caches
pacman -S --needed --noconfirm base-devel mingw-w64-i686-toolchain mingw-w64-x86_64-toolchain mingw-w64-clang-x86_64-toolchain git
pacman -Scc --noconfirm

# Make the msys2 tree ready for a git update
cd /
echo '* binary' > .gitattributes
find . -xdev -type d -empty -exec touch {}/.gitignore \; || true;
