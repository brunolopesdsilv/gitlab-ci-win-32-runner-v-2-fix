$ErrorActionPreference = "Stop"

$env:MSYSTEM = 'MSYS'
$env:CHERE_INVOKING = 'yes'

if ($env:FORCE_UPDATE) {
    git -C C:/msys64 clean -qxfdf
    git -C C:/msys64 reset HEAD --hard
    C:/msys64/usr/bin/bash -lc "pacman --noconfirm -Syyuu --overwrite '**'"
    C:/msys64/usr/bin/bash -lc "pacman --noconfirm -Syyuu --overwrite '**'"
    C:/msys64/usr/bin/bash -lc "bash -x ./ci/setup_msys2.sh"
    git -C C:/msys64 config user.email "foo@bar"
    git -C C:/msys64 config user.name "Foo Bar"
    git -C C:/msys64 add .
    git -C C:/msys64 commit --quiet --amend -m "auto-update"
    git -C C:/msys64 gc
}
