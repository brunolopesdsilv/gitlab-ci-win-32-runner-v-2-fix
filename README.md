# GitLab CI Windows Runner (v2)

[![pipeline status](https://gitlab.gnome.org/creiter/gitlab-ci-win32-runner-v2/badges/master/pipeline.svg)](https://gitlab.gnome.org/creiter/gitlab-ci-win32-runner-v2/-/commits/master)

Please file a bug here if something is not working.

Short overview:

* It only runs jobs tagged as `win32-ps`
* powershell.exe is used to execute commands in ".gitlab-ci.yml"
* When starting CWD is in the git repo
* The machine runs Windows Server 2016

Software installed:

* git (in PATH)
* ninja (in PATH)
* vswhere (in PATH)
* Python 3.8/3.9/3.10/3.11/3.12 (3.8 is in PATH)
* cmake (in PATH)
* Visual Studio 2017 C++ (you find it..)
* Visual Studio 2019 C++ (you find it..)
* MSYS2 (C:/msys64)

State:

* Unlike the docker runners this runner preserves state between runs
* The only exception is C:/msys64, which gets reset each run

How to use it:

* Best to test things on a real Windows machine/VM first with MSYS2 installed
* See the `example-mingw` jobs in this repo for how to use MSYS2
* For a working example see https://gitlab.gnome.org/GNOME/pygobject

See [SETUP.md](SETUP.md) for how to set up a CI server similar to this.

Porting from the v1 runner:

* Instead of cmd.exe everything is executed in powershell

Changelog:

* 2024-05-19: Added Python 3.9, 3.10, 3.11 and 3.12 (Python 3.8 is the default)
* 2023-05-14: Added Python 3.8 and removed Python 3.7
